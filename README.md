PHP Webmention Client with file-based cache

## Programmatic Idea
 * verify sourceURL
 * use the env var WEBMENTION_CACHE_DIR OR /tmp/.send.webmention.php.cache for caching endpoints & sent_mentions
 * crawl the "source" everytime, since it might have changed
 * cache webmention/pingbacks endpoints per targetURL
 * cache webmention/pingback sent
 * return json with cache status + pingback/wm status
## USE ME
### cli usage
   ```
   export WEBMENTION_CACHE_DIR=/path/to/cache
   php send_pingback_webmention.php target=https://my.site.com/my-post-with-links-outgoing
   ```
### web usage
   * change web environment to  set `WEBMENTION_CACHE_DIR=/path/to/cache`
   ```
   curl https://my.sending.domain/where/is/it/send_pingback_webmention.php?target=https://my.site.com/my-post-with-links-outgoing
   ```


---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/webmention_send_client.php/README.md/logo.jpg" width="480" height="270"/></div></a>

