<?php
require_once("/".dirname(__FILE__)."/"."./mention-client-php/src/IndieWeb/MentionClient.php");
require_once("/".dirname(__FILE__)."/"."./mention-client-php/vendor/autoload.php");
if(!function_exists("startsWith")) {
    function startsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        return substr( $haystack, 0, $length ) === $needle;
   }
}
if(!function_exists("endsWith")) {
   function endsWith( $haystack, $needle ) {
       $length = strlen( $needle );
       if( !$length ) {
           return true;
       }
       return substr( $haystack, -$length ) === $needle;
   } 
}

function acceptURL($URL) {
    if(
        startsWith($URL, "http:")
        ||
        startsWith($URL, "https:")
    ) {
        return true;
    } else {
    return false;
    }
}

$user_agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36";
// cache endpoints 12h ( target page might change )
$cachetime_endpoint_sec=12 * 3600;

//cache sent webmentions for one week
$cachetime_pb_sec= 7 * 24 * 3600;
$cachetime_wm_sec= 7 * 24 * 3600;
//accept cached sources if newer than 2h
$cachetime_pages=7200;

$outstatus=array();
$outstatus["cached"]=array();
$outstatus["pulled"]=array();

foreach ($argv as $arg) {
    $e=explode("=",$arg);
    if(count($e)==2)
        $_GET[$e[0]]=$e[1];
    else   
        $_GET[$e[0]]=0;
}


if(!isset($_GET["target"])) {
    if(!isset($outstatus["errors"])) { $outstatus["errors"]=array(); }
    array_push($outstatus["errors"],"no_target_url");
//    die("ERROR: NO target= NO FUN!");
}


//MAKE SURE THE CACHE DIR IS ONLY SET BY environment
//$cachedir=getenv('WEBMENTION_CACHE_DIR', true) ?: getenv('WEBMENTION_CACHE_DIR') // would get them from fpm/cgi as well
$cachedir=getenv('WEBMENTION_CACHE_DIR', true) ?: "NOTSET" ;
if(!isset($_GET["cachedir"])) {
    $outstatus["cachedir"]="def";
    $cachedir="/tmp/.send.webmention.php.cache";
} else {
    $outstatus["cachedir"]="set";
    //$cachedir=$_GET["crawldir"];
}



if (!file_exists($cachedir)) {
    mkdir($cachedir, 0777, true);
}
if($cachedir=="NOTSET"||$cachedir==""|| !file_exists($cachedir) || !is_dir($cachedir) ) {
    if(!isset($outstatus["errors"])) { $outstatus["errors"]=array(); }
    array_push($outstatus["errors"],"cache_dir_setup_failed");
    die("cache dir not set");
}

$client = new IndieWeb\MentionClient();

if(isset($_GET["target"]) && !acceptURL($_GET["target"])) {
    if(!isset($outstatus["errors"])) { $outstatus["errors"]=array(); }
    array_push($outstatus["errors"],"target_invalid");
}



// main action 
if(!isset($outstatus["errors"])) {

$sourceURL=$_GET["target"];
// we accept the cached file not found error and just get the page , but  we return it in the status

$use_cached_version=false;

if(isset($_GET["cached_source_file"]) && !file_exists($_GET["cached_source_file"]) ) {
    if(!isset($outstatus["errors"])) { $outstatus["errors"]=array(); }
    array_push($outstatus["errors"],"cached_source_file_missing");
} else {
    $use_cached_version=true;
}

if( $use_cached_version && 
              ( file_exists($_GET["cached_source_file"]) && (time()-filemtime($_GET["cached_source_file"])< $cachetime_pages ) )
              ) {
    $html=file_get_contents( $_GET["cached_source_file"] );
} else {
    if ( file_exists($_GET["cached_source_file"]) && (time()-filemtime($_GET["cached_source_file"]) > $cachetime_pages ) ) {
        unlink($_GET["cached_source_file"]);
    }
    $arrContextOptions=array(
        "http" => array(
            "method" => "GET",
            "header" => 
                "Content-Type: application/xml; charset=utf-8;\r\n".
                "User-Agent: ".$user_agent."\r\n",
            "ignore_errors" => true,
            "timeout" => (float)42.0,
         //   "content" => $strRequestXML,
        ),
        "ssl"=>array(
            "allow_self_signed"=>true,
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );
    $html=file_get_contents( $sourceURL , false, stream_context_create($arrContextOptions) );
}






$sumlockfile=$cachedir."/.pagesum_".md5($sourceURL);

$pagesum=md5($html);
$page_changed=false;

$outstatus["page_changed"]="false";


if( !file_exists($sumlockfile) || 
          ( file_exists($sumlockfile) && ( $pagesum != file_get_contents($sumlockfile) )  )  ) {
  $page_changed=true;
  $outstatus["page_changed"]="true";
} 

$allurls = $client->findOutgoingLinks($html);
IndieWeb\MentionClient::setUserAgent($user_agent);

// prevent self-sending
$urls=array();
foreach ( $allurls AS $currurl ) {
    if(  
        parse_url($currurl, PHP_URL_HOST) != parse_url($sourceURL, PHP_URL_HOST) 
      ) {
          array_push($urls,$currurl);
      }
}
$outstatus["url_count"]=count($urls);
$counter=0;
$outstatus["CountWebmentions"]=0;
$outstatus["CountPingbacks"]=0;
$outstatus["results"]=array();
foreach ( $urls AS $targetURL ) {
$outstatus["results"][$counter]=array();
$outstatus["results"][$counter]["url"]=$targetURL;


//WEBMENTION
$wm_endpoint_lock=$cachedir."/.webmentionurl_".md5($targetURL);

if( ( file_exists($wm_endpoint_lock) && (time()-filemtime($wm_endpoint_lock) < $cachetime_endpoint_sec ) ) ) {
    // detected endpoint less than 12h ago
    $wmendpoint =  file_get_contents($wm_endpoint_lock);
    array_push($outstatus["results"][$counter]["cached"],"wm_e_"+$counter);
} else {
    if(file_exists($wm_endpoint_lock)) {
        unlink($wm_endpoint_lock);
    }
    $wmendpoint = $client->discoverWebmentionEndpoint($targetURL);
    array_push($outstatus["results"][$counter]["pulled"],"wm_e_"+$counter);
}
if($wmendpoint)  {
    if(acceptURL($wmendpoint)) {
         file_put_contents($wm_endpoint_lock,$wmendpoint);
    }
    $wm_lock=$cachedir."/.webmention_sent_from_".md5($sourceURL)."_to_".md5($targetURL);
    if( ( file_exists($wm_lock) && (time()-filemtime($wm_lock) < $cachetime_wm_sec ) 
           &&
           $page_changed==false )
      ) {
        // we sent this thingy out in the last 12h and the page did not change since that
        $response=file_get_contents($wm_lock);
        $outstatus["results"][$counter]["webmention"]="OK:".$response." (CACHED)";
        array_push($outstatus["results"][$counter]["cached"],"wm");
    } else {
        array_push($outstatus["results"][$counter]["pulled"],"wm");
        if(file_exists($wm_lock)) { unlink($wm_lock) ; }
        //send webmention thingy
        $response = IndieWeb\MentionClient::sendWebmentionToEndpoint($wmendpoint, $sourceURL, $targetURL);
        //$client->sendPingback($sourceURL, $targetURL);
        //print_r($response);echo "WM";print_r($wmendpoint);
        if($response["code"] < 205 && $response["code"] > 199 ) {
            $outstatus["results"][$counter]["webmention"]="OK:".$response["code"];
            file_put_contents($wm_lock,$response["code"]);
        } else {
            $outstatus["results"][$counter]["webmention"]="ERROR:".$response["code"];
        }
    }
}  else { 
    //$outstatus["results"][$counter]["webmention"]="NOT_FOUND:no_endpoint";
    $outstatus["results"][$counter]["webmention"]="NOT_FOUND";
} // end WEBMENTION

//PINGBACK
$pb_endpoint_lock=$cachedir."/.pingbackurl_".md5($targetURL);
if(file_exists($pb_endpoint_lock) && (time()-filemtime($pb_endpoint_lock) < $cachetime_endpoint_sec ) ) {
    // detected endpoint less than 12h ago
    $pbendpoint =  file_get_contents($pb_endpoint_lock);
    array_push($outstatus["results"][$counter]["cached"],"pb_e_"+$counter);
} else {
    if(file_exists($pb_endpoint_lock)) {
        unlink($pb_endpoint_lock);
    }
    $pbendpoint = $client->discoverPingbackEndpoint($targetURL);
    array_push($outstatus["results"][$counter]["pulled"],"pb_e_"+$counter);
}

if($pbendpoint)  {
    if(acceptURL($pbendpoint)) {
        file_put_contents($pb_endpoint_lock,$pbendpoint);
   }
   $pb_lock=$cachedir."/.pingback_sent_from_".md5($sourceURL)."_to_".md5($targetURL);
   if( ( file_exists($pb_lock) && (time()-filemtime($pb_lock)< $cachetime_pb_sec ) 
   &&
   $page_changed==false )
) {
       // we sent this thingy out in the last 12h and the page did not change since that
       $response=false;
       if(file_get_contents($pb_lock)=="OK") {
        $response=true;
       }
       $outstatus["results"][$counter]["pingback"]="OK (CACHED)";
       array_push($outstatus["results"][$counter]["cached"],"pb");
    } else {
        array_push($outstatus["results"][$counter]["pulled"],"pb");
       if(file_exists($pb_lock)) { unlink($pb_lock) ; }
       //send pingback thingy
       //$client->sendPingback($sourceURL, $targetURL);
       $response = IndieWeb\MentionClient::sendPingbackToEndpoint($pbendpoint, $sourceURL, $targetURL);
       //$client->sendPingback($sourceURL, $targetURL);
       //print_r($response);echo "PB";print_r($pbendpoint);
       if($response) {
           $outstatus["CountPingbacks"]=$outstatus["CountPingbacks"]+1;
           $outstatus["results"][$counter]["pingback"]="OK";
           file_put_contents($pb_lock,"OK");
       } else {
           $outstatus["results"][$counter]["pingback"]="ERROR:".$response["code"]."URL :";
       }
   }
} else { 
        //$outstatus["results"][$counter]["pingback"]="NOT_FOUND:no_endpoint";
        $outstatus["results"][$counter]["pingback"]="NOT_FOUND";
} // end PINGBACK
$counter=$counter+1;
} // end foreach
} // end errors unset before
if(isset($page_changed) && $page_changed) {
    file_put_contents($sumlockfile,$pagesum);
}

print(json_encode(array_filter($outstatus),JSON_PRETTY_PRINT))
?>
